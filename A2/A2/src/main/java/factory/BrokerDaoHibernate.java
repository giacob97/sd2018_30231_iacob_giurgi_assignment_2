package factory;

import connections.HibernateConnection;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import persistence.Broker;
import org.hibernate.Transaction;

import java.util.List;

public class BrokerDaoHibernate implements FactoryInterface<Broker> {

    private SessionFactory sessionFactory;
    private HibernateConnection hibernateConnection = new HibernateConnection();

    public List<Broker> findAll() {
        sessionFactory = hibernateConnection.initialize();
        Session session = sessionFactory.openSession();
        List<Broker> brokers = session.createCriteria(Broker.class).list();
        session.close();
        sessionFactory.close();
        return brokers;

    }

    public Broker insert(Broker broker) {
        sessionFactory = hibernateConnection.initialize();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.save(broker);
        transaction.commit();
        session.close();
        sessionFactory.close();
        return broker;
    }

    public Broker findById(int id) {
        sessionFactory = hibernateConnection.initialize();
        Session session = sessionFactory.openSession();
        Broker b = session.get(Broker.class,id);
        session.close();
        sessionFactory.close();
        return b;
    }

    public Broker findByUsername(String username) {
        sessionFactory = hibernateConnection.initialize();
        Session session = sessionFactory.openSession();
        Broker b = (Broker)session.createCriteria(Broker.class).add(Restrictions.eq("username",username)).uniqueResult();
        session.close();
        sessionFactory.close();
        return b;
    }

    public Broker update(Broker broker) {
        sessionFactory = hibernateConnection.initialize();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.update(broker);
        transaction.commit();
        session.close();
        sessionFactory.close();
        return broker;
    }

    public Broker delete(int id) {
        Broker b = this.findById(id);
        System.out.println(b.toString());
        sessionFactory = hibernateConnection.initialize();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.delete(b);
        transaction.commit();
        session.close();
        sessionFactory.close();
        return null;
    }
}
