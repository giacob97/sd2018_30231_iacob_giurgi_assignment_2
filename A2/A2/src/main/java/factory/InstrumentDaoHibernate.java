package factory;

import connections.HibernateConnection;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import persistence.Instrument;

import java.util.List;
@SuppressWarnings({"all"})
public class InstrumentDaoHibernate implements FactoryInterface<Instrument> {

    private SessionFactory sessionFactory;
    private HibernateConnection hibernateConnection = new HibernateConnection();

    public List<Instrument> findAll() {
        sessionFactory = hibernateConnection.initialize();
        Session session = sessionFactory.openSession();
        List<Instrument> instruments = session.createCriteria(Instrument.class).list();
        session.close();
        sessionFactory.close();
        return instruments;
    }

    public Instrument insert(Instrument instrument) {
        sessionFactory = hibernateConnection.initialize();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.save(instrument);
        transaction.commit();
        session.close();
        sessionFactory.close();
        return instrument;
    }

    public Instrument findById(int id) {
        sessionFactory = hibernateConnection.initialize();
        Session session = sessionFactory.openSession();
        Instrument instrument = session.get(Instrument.class,id);
        session.close();
        sessionFactory.close();
        return instrument;
    }

    public Instrument update(Instrument instrument) {
        sessionFactory = hibernateConnection.initialize();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.update(instrument);
        transaction.commit();
        session.close();
        sessionFactory.close();
        return instrument;
    }

    public Instrument delete(int id) {
        Instrument instrument = this.findById(id);
        sessionFactory = hibernateConnection.initialize();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.delete(instrument);
        transaction.commit();
        session.close();
        sessionFactory.close();
        return null;
    }

    @Override
    public Instrument findByUsername(String username) {
        return null;
    }
}
