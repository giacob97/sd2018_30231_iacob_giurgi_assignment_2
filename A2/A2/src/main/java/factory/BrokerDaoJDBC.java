package factory;

import connections.SQLConnection;
import persistence.Broker;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
@SuppressWarnings({"all"})
public class BrokerDaoJDBC implements FactoryInterface<Broker> {

    private static final String insertStatementString = "INSERT INTO broker (username,password,fund) VALUES (?,?,?)";
    private static final String deleteStatementString = "delete from broker where idbroker =? ";
    private static final String findByIdStatement = "SELECT * FROM broker where idbroker=? ";
    private static final String findByIdUsername = "SELECT * FROM broker where username=? ";
    private static final String updateStatementString = "UPDATE broker SET fund = ? where idbroker=?";
    private static final String getLists = "SELECT * FROM broker";
    protected static final Logger LOGGER = Logger.getLogger(BrokerDaoJDBC.class.getName());

    public BrokerDaoJDBC() {
    }

    public Broker insert(Broker broker) {
        Connection dbConnection = (Connection) SQLConnection.getConnection();
        PreparedStatement insertStatement = null;
        try {

            insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
            insertStatement.setString(1, broker.getUsername());
            insertStatement.setString(2, broker.getPassword());
            insertStatement.setDouble(3, 1000000);
            insertStatement.executeUpdate();

        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "User:insert " + e.getMessage());
        } finally {
            SQLConnection.close(insertStatement);
            SQLConnection.close(dbConnection);
        }
        return broker;
    }

    public Broker findById(int id) {
        Connection dbConnection = (Connection) SQLConnection.getConnection();
        PreparedStatement findStatement = null;
        Broker broker = new Broker();
        try {

            findStatement = dbConnection.prepareStatement(findByIdStatement, Statement.RETURN_GENERATED_KEYS);
            findStatement.setInt(1,id);
            ResultSet resultSet = findStatement.executeQuery();
            resultSet.next();
            broker.setIdBroker(resultSet.getInt("idbroker"));
            broker.setUsername(resultSet.getString("username"));
            broker.setPassword(resultSet.getString("password"));
            broker.setFund(resultSet.getDouble("fund"));

        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "User:findById" + e.getMessage());
        } finally {
            SQLConnection.close(findStatement);
            SQLConnection.close(dbConnection);
        }
        return broker;
    }

    public Broker findByUsername(String username) {
        Connection dbConnection = (Connection) SQLConnection.getConnection();
        PreparedStatement findStatement = null;
        Broker broker = new Broker();
        try {

            findStatement = dbConnection.prepareStatement(findByIdUsername, Statement.RETURN_GENERATED_KEYS);
            findStatement.setString(1,username);
            ResultSet resultSet = findStatement.executeQuery();
            resultSet.next();
            broker.setIdBroker(resultSet.getInt("idbroker"));
            broker.setUsername(resultSet.getString("username"));
            broker.setPassword(resultSet.getString("password"));
            broker.setFund(resultSet.getDouble("fund"));

        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "User:findById" + e.getMessage());
        } finally {
            SQLConnection.close(findStatement);
            SQLConnection.close(dbConnection);
        }
        return broker;
    }

    public Broker update(Broker broker) {
        Connection dbConnection = (Connection) SQLConnection.getConnection();
        PreparedStatement updateStatement = null;
        try {

            updateStatement = dbConnection.prepareStatement(updateStatementString, Statement.RETURN_GENERATED_KEYS);
            updateStatement.setDouble(1, broker.getFund());
            updateStatement.setInt(2,broker.getIdBroker());
            updateStatement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "User:update " + e.getMessage());
        } finally {
            SQLConnection.close(updateStatement);
            SQLConnection.close(dbConnection);
        }
        return broker;
    }

    public Broker delete(int id) {
        Connection dbConnection = (Connection) SQLConnection.getConnection();
        PreparedStatement deleteStatement = null;
        Broker broker = new Broker();
        try {

            deleteStatement = dbConnection.prepareStatement(deleteStatementString, Statement.RETURN_GENERATED_KEYS);
            deleteStatement.setInt(1,id);
            deleteStatement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "User:Delete" + e.getMessage());
        } finally {
            SQLConnection.close(deleteStatement);
            SQLConnection.close(dbConnection);
        }
        return broker;
    }

    public List<Broker> findAll() {
        List<Broker> brokers = new ArrayList<Broker>();
        Connection dbConnection = (Connection) SQLConnection.getConnection();
        PreparedStatement findStatement = null;
        try {

            findStatement = dbConnection.prepareStatement(getLists, Statement.RETURN_GENERATED_KEYS);
            ResultSet resultSet = findStatement.executeQuery();
            while(resultSet.next()) {
                Broker broker = new Broker();
                broker.setIdBroker(resultSet.getInt("idbroker"));
                broker.setUsername(resultSet.getString("username"));
                broker.setPassword(resultSet.getString("password"));
                broker.setFund(resultSet.getDouble("fund"));
                brokers.add(broker);
            }
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "User:FindAll" + e.getMessage());
        } finally {
            SQLConnection.close(findStatement);
            SQLConnection.close(dbConnection);
        }
        return brokers;

    }


}
