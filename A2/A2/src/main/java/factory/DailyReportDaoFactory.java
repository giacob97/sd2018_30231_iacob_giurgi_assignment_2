package factory;

public class DailyReportDaoFactory {

    public EndOfDayInterface getDailyReport(String daoType){
        if(daoType.equals("J")){
            return new EndOfDayDaoJDBC();
        }
        else {
            if(daoType.equals("H")) {
                return new EndOfDayDaoHibernate();
            }
            else{
                return  null;
            }
        }
    }

}
