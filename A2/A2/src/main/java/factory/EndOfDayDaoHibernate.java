package factory;

import connections.HibernateConnection;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import persistence.DailyReport;

import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.List;

public class EndOfDayDaoHibernate implements EndOfDayInterface {


    private SessionFactory sessionFactory;
    private HibernateConnection hibernateConnection = new HibernateConnection();

    @Override
    public boolean endOfDay(String today) {
        sessionFactory = hibernateConnection.initialize();
        Session session = sessionFactory.openSession();
        boolean found = false;
        try {
            Date todayDate = new SimpleDateFormat("yyyy-MM-dd").parse(today);
            DailyReport dailyReport = (DailyReport) session.createCriteria(DailyReport.class).add(Restrictions.eq("todayDate",todayDate)).uniqueResult();
            session.close();
            sessionFactory.close();
            if(dailyReport.getIdDailyReport() > 0)
                return true;
            else{
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public List<DailyReport> findAll() {
        sessionFactory = hibernateConnection.initialize();
        Session session = sessionFactory.openSession();
        List<DailyReport> dailyReports = session.createCriteria(DailyReport.class).list();
        session.close();
        sessionFactory.close();
        return dailyReports;
    }

    @Override
    public DailyReport insert(DailyReport dailyReport) {
        sessionFactory = hibernateConnection.initialize();
        Session session = sessionFactory.openSession();
        org.hibernate.Transaction transactionHib = session.beginTransaction();
        session.save(dailyReport);
        transactionHib.commit();
        session.close();
        sessionFactory.close();
        return dailyReport;
    }

    @Override
    public DailyReport findById(int id) {
        sessionFactory = hibernateConnection.initialize();
        Session session = sessionFactory.openSession();
        DailyReport dailyReport = session.get(DailyReport.class,id);
        session.close();
        sessionFactory.close();
        return dailyReport;
    }

    @Override
    public DailyReport update(DailyReport dailyReport) {
        sessionFactory = hibernateConnection.initialize();
        Session session = sessionFactory.openSession();
        org.hibernate.Transaction transactionHib = session.beginTransaction();
        session.update(dailyReport);
        transactionHib.commit();
        session.close();
        sessionFactory.close();
        return dailyReport;
    }

    @Override
    public DailyReport delete(int id) {
         DailyReport dailyReport = this.findById(id);
        sessionFactory = hibernateConnection.initialize();
        Session session = sessionFactory.openSession();
        org.hibernate.Transaction transaction = session.beginTransaction();
        session.delete(dailyReport);
        transaction.commit();
        session.close();
        sessionFactory.close();
        return dailyReport;
    }

    @Override
    public DailyReport findByUsername(String username) {
        return null;
    }
}
