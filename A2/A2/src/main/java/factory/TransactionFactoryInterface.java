package factory;

import persistence.Transaction;

import java.util.Date;
import java.util.List;

public interface TransactionFactoryInterface extends FactoryInterface<Transaction> {

    public int getNumberOfSharesPerInstrument(int id,int idBroker);
    public List<Transaction> getDailyTransactions(String date);
    public List<Transaction> getTransactionsFromInterval(String startDate, String endDate,int idBroker);

}
