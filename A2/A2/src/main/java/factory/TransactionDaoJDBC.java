package factory;

import connections.SQLConnection;

import persistence.Transaction;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
@SuppressWarnings("all")
public class TransactionDaoJDBC implements TransactionFactoryInterface {

    private static final String insertStatementString = "INSERT INTO transaction (date,idBroker,idInstrument,quantity,price,type,newFund) VALUES (?,?,?,?,?,?,?)";
    private static final String deleteStatementString = "delete from transaction where idtransaction =? ";
    private static final String findByIdStatement = "SELECT * FROM transaction where idtransaction=? ";
    private static final String updateStatementString = "UPDATE transaction SET quantity = ? , price = ? where idtransaction=?";
    private static final String getLists = "SELECT * FROM transaction";
    private static final String getListsPerInstrument = "SELECT * FROM transaction where idInstrument=? and idBroker=?";
    private static final String getDailyTransactions = "SELECT * FROM transaction where date=?";
    private static final String getIntervalTransactions = "SELECT * FROM transaction where date>=? and date<=? and idBroker=?";
    protected static final Logger LOGGER = Logger.getLogger(TransactionDaoJDBC.class.getName());



    public Transaction insert(Transaction transaction) {
        Connection dbConnection = (Connection) SQLConnection.getConnection();
        PreparedStatement insertStatement = null;
        try {

            insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
            insertStatement.setDate(1,new java.sql.Date(transaction.getDate().getTime()));
            insertStatement.setInt(2,transaction.getIdbroker());
            insertStatement.setInt(3,transaction.getIdInstrument());
            insertStatement.setInt(4,transaction.getQuantity());
            insertStatement.setInt(5,transaction.getPrice());
            insertStatement.setString(6,transaction.getType());
            insertStatement.setInt(7,transaction.getNewFund());
            insertStatement.executeUpdate();

        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "Transaction:insert " + e.getMessage());
        } finally {
            SQLConnection.close(insertStatement);
            SQLConnection.close(dbConnection);
        }
        return transaction;
    }

    public Transaction findById(int id) {
        Connection dbConnection = (Connection) SQLConnection.getConnection();
        PreparedStatement findStatement = null;
        Transaction transaction = new Transaction();
        try {

            findStatement = dbConnection.prepareStatement(findByIdStatement, Statement.RETURN_GENERATED_KEYS);
            findStatement.setInt(1,id);
            ResultSet resultSet = findStatement.executeQuery();
            resultSet.next();
            transaction.setIdTransaction(resultSet.getInt("idtransaction"));
            transaction.setDate(resultSet.getDate("date"));
            transaction.setIdbroker(resultSet.getInt("idBroker"));
            transaction.setIdInstrument(resultSet.getInt("idInstrument"));
            transaction.setQuantity(resultSet.getInt("quantity"));
            transaction.setPrice(resultSet.getInt("price"));
            transaction.setType(resultSet.getString("type"));
            transaction.setNewFund(resultSet.getInt("newFund"));
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "Transaction:findById" + e.getMessage());
        } finally {
            SQLConnection.close(findStatement);
            SQLConnection.close(dbConnection);
        }
        return transaction;
    }

    public Transaction update(Transaction transaction) {
        Connection dbConnection = (Connection) SQLConnection.getConnection();
        PreparedStatement updateStatement = null;
        try {

            updateStatement = dbConnection.prepareStatement(updateStatementString, Statement.RETURN_GENERATED_KEYS);
            updateStatement.setInt(1, transaction.getQuantity());
            updateStatement.setInt(2,transaction.getPrice());
            updateStatement.setInt(3,transaction.getIdTransaction());
            updateStatement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "Transaction:update " + e.getMessage());
        } finally {
            SQLConnection.close(updateStatement);
            SQLConnection.close(dbConnection);
        }
        return transaction;
    }

    public Transaction delete(int id) {
        Connection dbConnection = (Connection) SQLConnection.getConnection();
        PreparedStatement deleteStatement = null;
        try {

            deleteStatement = dbConnection.prepareStatement(deleteStatementString, Statement.RETURN_GENERATED_KEYS);
            deleteStatement.setInt(1,id);
            deleteStatement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "Transaction:Delete" + e.getMessage());
        } finally {
            SQLConnection.close(deleteStatement);
            SQLConnection.close(dbConnection);
        }
        return null;
    }

    @Override
    public Transaction findByUsername(String username) {
        return null;
    }


    public List<Transaction> findAll() {
        List<Transaction> transactions = new ArrayList<>();
        Connection dbConnection = (Connection) SQLConnection.getConnection();
        PreparedStatement findStatement = null;
        try {

            findStatement = dbConnection.prepareStatement(getLists, Statement.RETURN_GENERATED_KEYS);

            ResultSet resultSet = findStatement.executeQuery();
            while(resultSet.next()) {
                Transaction transaction = new Transaction();
                transaction.setIdTransaction(resultSet.getInt("idtransaction"));
                transaction.setDate(resultSet.getDate("date"));
                transaction.setIdbroker(resultSet.getInt("idBroker"));
                transaction.setIdInstrument(resultSet.getInt("idInstrument"));
                transaction.setQuantity(resultSet.getInt("quantity"));
                transaction.setPrice(resultSet.getInt("price"));
                transaction.setType(resultSet.getString("type"));
                transaction.setNewFund(resultSet.getInt("newFund"));
                transactions.add(transaction);
            }
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "Transaction:FindAll" + e.getMessage());
        } finally {
            SQLConnection.close(findStatement);
            SQLConnection.close(dbConnection);
        }
        return transactions;
    }

    @Override
    public int getNumberOfSharesPerInstrument(int id,int idBroker) {
         int sum = 0;
        List<Transaction> transactions = new ArrayList<>();
        Connection dbConnection = (Connection) SQLConnection.getConnection();
        PreparedStatement findStatement = null;
        try {

            findStatement = dbConnection.prepareStatement(getListsPerInstrument, Statement.RETURN_GENERATED_KEYS);
            findStatement.setInt(1,id);
            findStatement.setInt(2,idBroker);
            ResultSet resultSet = findStatement.executeQuery();
            while(resultSet.next()) {
                Transaction transaction = new Transaction();
                transaction.setIdTransaction(resultSet.getInt("idtransaction"));
                transaction.setDate(resultSet.getDate("date"));
                transaction.setIdbroker(resultSet.getInt("idBroker"));
                transaction.setIdInstrument(resultSet.getInt("idInstrument"));
                transaction.setQuantity(resultSet.getInt("quantity"));
                transaction.setPrice(resultSet.getInt("price"));
                transaction.setType(resultSet.getString("type"));
                transaction.setNewFund(resultSet.getInt("newFund"));
                transactions.add(transaction);
            }
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "Transaction:FindAll" + e.getMessage());
        } finally {
            SQLConnection.close(findStatement);
            SQLConnection.close(dbConnection);
        }
         for(Transaction t : transactions){
            if(t.getType().equals("buy")){
                sum += t.getQuantity();
            }
            else{
                sum -= t.getQuantity();
            }
         }
         return sum;
    }

    @Override
    public List<Transaction> getDailyTransactions(String date) {
        List<Transaction> transactions = new ArrayList<>();
        Connection dbConnection = (Connection) SQLConnection.getConnection();
        PreparedStatement findStatement = null;
        try {

            findStatement = dbConnection.prepareStatement(getDailyTransactions, Statement.RETURN_GENERATED_KEYS);
            findStatement.setString(1, date);
            ResultSet resultSet = findStatement.executeQuery();
            while(resultSet.next()) {
                Transaction transaction = new Transaction();
                transaction.setIdTransaction(resultSet.getInt("idtransaction"));
                transaction.setDate(resultSet.getDate("date"));
                transaction.setIdbroker(resultSet.getInt("idBroker"));
                transaction.setIdInstrument(resultSet.getInt("idInstrument"));
                transaction.setQuantity(resultSet.getInt("quantity"));
                transaction.setPrice(resultSet.getInt("price"));
                transaction.setType(resultSet.getString("type"));
                transaction.setNewFund(resultSet.getInt("newFund"));
                transactions.add(transaction);
            }
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "Transaction:Find Daily Transactions" + e.getMessage());
        } finally {
            SQLConnection.close(findStatement);
            SQLConnection.close(dbConnection);
        }
        return transactions;
    }

    @Override
    public List<Transaction> getTransactionsFromInterval(String startDate, String endDate, int idBroker) {
        List<Transaction> transactions = new ArrayList<>();
        Connection dbConnection = (Connection) SQLConnection.getConnection();
        PreparedStatement findStatement = null;
        try {

            findStatement = dbConnection.prepareStatement(getIntervalTransactions, Statement.RETURN_GENERATED_KEYS);
            findStatement.setString(1, startDate);
            findStatement.setString(2, endDate);
            findStatement.setInt(3,idBroker);
            ResultSet resultSet = findStatement.executeQuery();
            while(resultSet.next()) {
                Transaction transaction = new Transaction();
                transaction.setIdTransaction(resultSet.getInt("idtransaction"));
                transaction.setDate(resultSet.getDate("date"));
                transaction.setIdbroker(resultSet.getInt("idBroker"));
                transaction.setIdInstrument(resultSet.getInt("idInstrument"));
                transaction.setQuantity(resultSet.getInt("quantity"));
                transaction.setPrice(resultSet.getInt("price"));
                transaction.setType(resultSet.getString("type"));
                transaction.setNewFund(resultSet.getInt("newFund"));
                transactions.add(transaction);
            }
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "Transaction:Find Interval Transactions" + e.getMessage());
        } finally {
            SQLConnection.close(findStatement);
            SQLConnection.close(dbConnection);
        }
        return transactions;
    }
}
