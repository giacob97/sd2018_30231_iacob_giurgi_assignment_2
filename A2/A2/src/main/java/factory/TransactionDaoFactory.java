package factory;

import persistence.Transaction;

public class TransactionDaoFactory {

    public FactoryInterface<Transaction> getTransaction(String daoType){
        if(daoType.equals("H")){
            return new TransactionDaoHibernate();
        }
        else {
            if (daoType.equals("J")) {
                return new TransactionDaoJDBC();
            }
            else return null;
        }
    }


    public TransactionFactoryInterface getTransactionForReports(String daoType){
        if(daoType.equals("H")){
            return new TransactionDaoHibernate();
        }
        else {
            if (daoType.equals("J")) {
                return new TransactionDaoJDBC();
            }
            else return null;
        }
    }

}
