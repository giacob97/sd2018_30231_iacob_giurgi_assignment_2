package factory;

import persistence.Instrument;

public class InstrumentDaoFactory {

    public FactoryInterface<Instrument> getInstrument(String daoType){
        if(daoType.equals("H")){
            return new InstrumentDaoHibernate();
        }
        else {
            if (daoType.equals("J")) {
                return new InstrumentDaoJDBC();
            }
            else return null;
        }
    }

}
