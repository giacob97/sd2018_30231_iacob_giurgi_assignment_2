package factory;

import connections.HibernateConnection;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import persistence.Instrument;
import persistence.Transaction;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SuppressWarnings("all")
public class TransactionDaoHibernate implements TransactionFactoryInterface {

    private SessionFactory sessionFactory;
    private HibernateConnection hibernateConnection = new HibernateConnection();

    @Override
    public List<Transaction> findAll() {
        sessionFactory = hibernateConnection.initialize();
        Session session = sessionFactory.openSession();
        List<Transaction> transactions = session.createCriteria(Transaction.class).list();
        session.close();
        sessionFactory.close();
        return transactions;
    }

    @Override
    public Transaction insert(Transaction transaction) {
        sessionFactory = hibernateConnection.initialize();
        Session session = sessionFactory.openSession();
        org.hibernate.Transaction transactionHib = session.beginTransaction();
        session.save(transaction);
        transactionHib.commit();
        session.close();
        sessionFactory.close();
        return transaction;
    }

    @Override
    public Transaction findById(int id) {
        sessionFactory = hibernateConnection.initialize();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.get(Transaction.class,id);
        session.close();
        sessionFactory.close();
        return transaction;
    }

    @Override
    public Transaction update(Transaction transaction) {
        sessionFactory = hibernateConnection.initialize();
        Session session = sessionFactory.openSession();
        org.hibernate.Transaction transactionHib = session.beginTransaction();
        session.update(transaction);
        transactionHib.commit();
        session.close();
        sessionFactory.close();
        return transaction;
    }

    @Override
    public Transaction delete(int id) {
        Transaction tr = this.findById(id);
        sessionFactory = hibernateConnection.initialize();
        Session session = sessionFactory.openSession();
        org.hibernate.Transaction transaction = session.beginTransaction();
        session.delete(tr);
        transaction.commit();
        session.close();
        sessionFactory.close();
        return null;
    }

    @Override
    public Transaction findByUsername(String username) {
        return null;
    }

    @Override
    public int getNumberOfSharesPerInstrument(int id,int idBroker) {
        int sum = 0;
        List<Transaction> allTransactions = this.findAll();
        List<Transaction> filteredTransactions = allTransactions.stream().filter(x -> x.getIdInstrument() == id).collect(Collectors.toList());
        List<Transaction> filteredTransactions2 = filteredTransactions.stream().filter(x -> x.getIdbroker()==idBroker).collect(Collectors.toList());
        for(Transaction t : filteredTransactions2){
            if(t.getType().equals("buy")){
                sum += t.getQuantity();
            }
            else{
                sum -= t.getQuantity();
            }
        }
        return sum;
    }

    @Override
    public List<Transaction> getDailyTransactions(String date) {
        sessionFactory = hibernateConnection.initialize();
        Session session = sessionFactory.openSession();
        try {
            Date datee = new SimpleDateFormat("yyyy-MM-dd").parse(date);
            List<Transaction> transactions = session.createCriteria(Transaction.class).add(Restrictions.eq("date", datee)).list();
            session.close();
            sessionFactory.close();
            return transactions;
        }catch (Exception e ){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Transaction> getTransactionsFromInterval(String startDate, String endDate, int idBroker) {
        sessionFactory = hibernateConnection.initialize();
        Session session = sessionFactory.openSession();
        try {
            Date sdatee = new SimpleDateFormat("yyyy-MM-dd").parse(startDate);
            Date edatee = new SimpleDateFormat("yyyy-MM-dd").parse(endDate);
            List<Transaction> transactions = session.createCriteria(Transaction.class).add(Restrictions.eq("idbroker", idBroker)).list();
            List<Transaction> filteredTransactions = transactions.stream().filter(x -> x.getDate().compareTo(sdatee) >= 0).collect(Collectors.toList());
            List<Transaction> filteredTransactions2 = filteredTransactions.stream().filter(x -> x.getDate().compareTo(sdatee) <= 0).collect(Collectors.toList());
            session.close();
            sessionFactory.close();
            return filteredTransactions2;
        }catch (Exception e ){
            e.printStackTrace();
        }
        return null;
    }
}
