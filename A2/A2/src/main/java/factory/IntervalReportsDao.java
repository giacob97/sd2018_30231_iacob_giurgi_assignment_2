package factory;

import persistence.Transaction;

import java.util.List;

public class IntervalReportsDao implements ReportInterface {
    @Override
    public List<Transaction> getTransactions(String typeOfConnection, String startDay, String endDay, int idBroker) {
        TransactionDaoFactory transactionDaoFactory = new TransactionDaoFactory();
        TransactionFactoryInterface transactionFactoryInterface = transactionDaoFactory.getTransactionForReports(typeOfConnection);
        return transactionFactoryInterface.getTransactionsFromInterval(startDay,endDay,idBroker);
    }
}
