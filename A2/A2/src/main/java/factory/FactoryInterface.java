package factory;

import java.util.ArrayList;
import java.util.List;

public interface FactoryInterface<T> {

    public List<T> findAll();
    public T insert(T t);
    public T findById(int id);
    public T update(T t);
    public T delete(int id);
    public T findByUsername(String username);
}
