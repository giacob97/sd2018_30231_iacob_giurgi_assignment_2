package factory;

import connections.SQLConnection;

import persistence.DailyReport;

import java.sql.*;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
@SuppressWarnings("all")
public class EndOfDayDaoJDBC implements EndOfDayInterface {

    private static final String insertStatementString = "INSERT INTO daily_report (date) VALUES (?)";
    private static final String deleteStatementString = "delete from daily_report where iddaily_report =? ";
    private static final String findByDate = "SELECT * FROM daily_report where date=? ";
    protected static final Logger LOGGER = Logger.getLogger(EndOfDayDaoJDBC.class.getName());

    @Override
    public boolean endOfDay(String today) {
        Connection dbConnection = (Connection) SQLConnection.getConnection();
        PreparedStatement findStatement = null;
        boolean found  = false;
        DailyReport dailyReport = new DailyReport();
        try {

            findStatement = dbConnection.prepareStatement(findByDate, Statement.RETURN_GENERATED_KEYS);
            findStatement.setString(1,today);
            ResultSet resultSet = findStatement.executeQuery();
            resultSet.next();
            dailyReport.setIdDailyReport(resultSet.getInt("iddaily_report"));
            dailyReport.setTodayDate(resultSet.getDate("date"));
            found = true;

        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "Daily Report:findByDate" + e.getMessage());
        } finally {
            SQLConnection.close(findStatement);
            SQLConnection.close(dbConnection);

        }
        return found;
    }

    @Override
    public List<DailyReport> findAll() {
        return null;
    }

    @Override
    public DailyReport insert(DailyReport dailyReport) {
        Connection dbConnection = (Connection) SQLConnection.getConnection();
        PreparedStatement insertStatement = null;
        try {

            insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
            insertStatement.setDate(1, new java.sql.Date(dailyReport.getTodayDate().getTime()));
            insertStatement.executeUpdate();

        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "Daily Report:insert " + e.getMessage());
        } finally {
            SQLConnection.close(insertStatement);
            SQLConnection.close(dbConnection);
        }
        return dailyReport;
    }

    @Override
    public DailyReport findById(int id) {
        return null;
    }

    @Override
    public DailyReport update(DailyReport dailyReport) {
        return null;
    }

    @Override
    public DailyReport delete(int id) {
        Connection dbConnection = (Connection) SQLConnection.getConnection();
        PreparedStatement deleteStatement = null;
        try {

            deleteStatement = dbConnection.prepareStatement(deleteStatementString, Statement.RETURN_GENERATED_KEYS);
            deleteStatement.setInt(1,id);
            deleteStatement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "Daily Report:Delete" + e.getMessage());
        } finally {
            SQLConnection.close(deleteStatement);
            SQLConnection.close(dbConnection);
        }
        return new DailyReport();
    }

    @Override
    public DailyReport findByUsername(String username) {
        return null;
    }
}
