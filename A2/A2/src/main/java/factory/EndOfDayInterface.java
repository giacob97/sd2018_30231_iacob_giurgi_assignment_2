package factory;

import persistence.DailyReport;

public interface EndOfDayInterface extends FactoryInterface<DailyReport> {

    public boolean endOfDay(String today);

}
