package factory;

import persistence.*;

import java.util.List;

public interface ReportInterface {


    public List<Transaction> getTransactions(String typeOfConnection ,String startDay,String endDay,int idBroker);

}
