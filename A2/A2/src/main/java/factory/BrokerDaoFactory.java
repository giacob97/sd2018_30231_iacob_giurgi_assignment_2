package factory;

import persistence.Broker;

public class BrokerDaoFactory {

    public FactoryInterface<Broker> getBroker(String daoType){
        if(daoType.equals("H")){
            return new BrokerDaoHibernate();
        }
        else {
            if (daoType.equals("J")) {
                return new BrokerDaoJDBC();
            }
            else return null;
        }
    }
}
