package factory;

import connections.SQLConnection;
import persistence.Instrument;


import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
@SuppressWarnings("all")
public class InstrumentDaoJDBC implements FactoryInterface<Instrument> {

    private static final String insertStatementString = "INSERT INTO instrument (name,price) VALUES (?,?)";
    private static final String deleteStatementString = "delete from instrument where idinstrument =? ";
    private static final String findByIdStatement = "SELECT * FROM instrument where idinstrument=? ";
    private static final String updateStatementString = "UPDATE instrument SET price = ? where idinstrument=?";
    private static final String getLists = "SELECT * FROM instrument";
    protected static final Logger LOGGER = Logger.getLogger(InstrumentDaoJDBC.class.getName());


    public Instrument insert(Instrument instrument) {
        Connection dbConnection = (Connection) SQLConnection.getConnection();
        PreparedStatement insertStatement = null;
        try {

            insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
            insertStatement.setString(1, instrument.getName());
            insertStatement.setInt(2, instrument.getPrice());
            insertStatement.executeUpdate();

        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "Instrument:insert " + e.getMessage());
        } finally {
            SQLConnection.close(insertStatement);
            SQLConnection.close(dbConnection);
        }
        return instrument;
    }

    public Instrument findById(int id) {
        Connection dbConnection = (Connection) SQLConnection.getConnection();
        PreparedStatement findStatement = null;
        Instrument instrument = new Instrument();
        try {

            findStatement = dbConnection.prepareStatement(findByIdStatement, Statement.RETURN_GENERATED_KEYS);
            findStatement.setInt(1,id);
            ResultSet resultSet = findStatement.executeQuery();
            resultSet.next();
            instrument.setIdInstrument(resultSet.getInt("idinstrument"));
            instrument.setName(resultSet.getString("name"));
            instrument.setPrice(resultSet.getInt("price"));

        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "User:findById" + e.getMessage());
        } finally {
            SQLConnection.close(findStatement);
            SQLConnection.close(dbConnection);
        }
        return instrument;
    }

    public Instrument update(Instrument instrument) {
        Connection dbConnection = (Connection) SQLConnection.getConnection();
        PreparedStatement updateStatement = null;
        try {

            updateStatement = dbConnection.prepareStatement(updateStatementString, Statement.RETURN_GENERATED_KEYS);
            updateStatement.setInt(1, instrument.getPrice());
            updateStatement.setInt(2,instrument.getIdInstrument());
            updateStatement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "User:insert " + e.getMessage());
        } finally {
            SQLConnection.close(updateStatement);
            SQLConnection.close(dbConnection);
        }
        return instrument;
    }

    public Instrument delete(int id) {
        Connection dbConnection = (Connection) SQLConnection.getConnection();
        PreparedStatement deleteStatement = null;
        Instrument instrument = new Instrument();
        try {

            deleteStatement = dbConnection.prepareStatement(deleteStatementString, Statement.RETURN_GENERATED_KEYS);
            deleteStatement.setInt(1,id);
            deleteStatement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "User:Delete" + e.getMessage());
        } finally {
            SQLConnection.close(deleteStatement);
            SQLConnection.close(dbConnection);
        }
        return instrument;
    }

    @Override
    public Instrument findByUsername(String username) {
        return null;
    }

    public List<Instrument> findAll() {
        List<Instrument> instruments = new ArrayList<Instrument>();
        Connection dbConnection = (Connection) SQLConnection.getConnection();
        PreparedStatement findStatement = null;
        try {

            findStatement = dbConnection.prepareStatement(getLists, Statement.RETURN_GENERATED_KEYS);
            ResultSet resultSet = findStatement.executeQuery();
            while(resultSet.next()) {
                Instrument instrument = new Instrument();
                instrument.setIdInstrument(resultSet.getInt("idinstrument"));
                instrument.setName(resultSet.getString("name"));
                instrument.setPrice(resultSet.getInt("price"));
                instruments.add(instrument);
            }
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "User:Delete" + e.getMessage());
        } finally {
            SQLConnection.close(findStatement);
            SQLConnection.close(dbConnection);
        }
        return instruments;

    }

}
