package persistence;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Entity
@Table(name = "daily_report")
public class DailyReport {

    @Id
    @Column(name = "iddaily_report")
    private int idDailyReport;

    @Column(name = "date")
    private Date todayDate;

    public DailyReport(){

    }

    public DailyReport(String date){
        this.todayDate = this.parseDate(date);
    }

    public Date parseDate(String stringToDate){
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        formatter.setLenient(false);
        try {
            Date date = formatter.parse(stringToDate);
            return date;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public int getIdDailyReport() {
        return idDailyReport;
    }

    public void setIdDailyReport(int idDailyReport) {
        this.idDailyReport = idDailyReport;
    }

    public Date getTodayDate() {
        return todayDate;
    }

    public void setTodayDate(Date todayDate) {
        this.todayDate = todayDate;
    }
}
