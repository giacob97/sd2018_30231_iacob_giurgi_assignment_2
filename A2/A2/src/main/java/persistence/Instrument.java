package persistence;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "instrument")
public class Instrument {

    @Id
    @Column(name = "idinstrument")
    private int idInstrument;

    @Column(name = "name")
    private String name;

    @Column(name = "price")
    private int price;

    @Column(nullable = true, name = "totalShares")
    private Integer totalShares;



    public Instrument(String name, int price) {
        this.name = name;
        this.price = price;
    }


    public Instrument(){

    }

    public int getIdInstrument() {
        return idInstrument;
    }

    public void setIdInstrument(int idInstrument) {
        this.idInstrument = idInstrument;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Integer getTotalShares() {
        return totalShares;
    }

    public void setTotalShares(int totalShares) {
        this.totalShares = totalShares;
    }


}
