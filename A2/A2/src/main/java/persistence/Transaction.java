package persistence;

import factory.BrokerDaoFactory;
import factory.BrokerDaoJDBC;
import factory.InstrumentDaoJDBC;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Entity
@Table(name = "transaction")
public class Transaction {

    @Id
    @Column(name = "idtransaction")
    private int idTransaction;

    @Column(name = "date")
    private Date date;

    @Column(name = "idBroker")
    private int idbroker;

    @Column(name = "idInstrument")
    private int idInstrument;

    @Column(name = "quantity")
    private int quantity;

    @Column(name = "price")
    private int price;

    @Column(name = "type")
    private String type;

    @Column(name = "newFund")
    private int newFund;

    public Transaction(String date,int idbroker, int idInstrument, int quantity, int price, String type,int newFund) {

        this.idbroker = idbroker;
        this.idInstrument = idInstrument;
        this.quantity = quantity;
        this.price = price;
        this.type = type;
        this.newFund = newFund;
        this.date = this.parseDate(date);
    }

    public Transaction(){

    }

    public Date parseDate(String stringToDate){
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        formatter.setLenient(false);
        try {
            Date date = formatter.parse(stringToDate);
            return date;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public int getIdTransaction() {
        return idTransaction;
    }

    public void setIdTransaction(int idTransaction) {
        this.idTransaction = idTransaction;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getIdbroker() {
        return idbroker;
    }

    public void setIdbroker(int idbroker) {
        this.idbroker = idbroker;
    }

    public int getIdInstrument() {
        return idInstrument;
    }

    public void setIdInstrument(int idInstrument) {
        this.idInstrument = idInstrument;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getNewFund() {
        return newFund;
    }

    public void setNewFund(int newFund) {
        this.newFund = newFund;
    }

    @Override
    public String toString(){
        InstrumentDaoJDBC ib = new InstrumentDaoJDBC();
        Instrument instrument = ib.findById(this.getIdInstrument());
        Broker broker = new BrokerDaoJDBC().findById(this.getIdbroker());
        return "Data: " + this.getDate() + ", Tip: " + this.getType() + ", Cantitate: " + this.getQuantity()
                + ", Pret: " + this.getPrice() + ", Instrument: " + instrument.getName() + ", Broker: " + broker.getUsername() + ", Noul fond: " + this.newFund;
    }

}

