package persistence;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "broker")
public class Broker {

    @Id
    @Column(name = "idbroker")
    private int idBroker;

    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;

    @Column(name = "fund")
    private double fund = 1000000;

    public Broker(){

    }

    public Broker(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public int getIdBroker() {
        return idBroker;
    }

    public void setIdBroker(int idBroker) {
        this.idBroker = idBroker;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public double getFund() {
        return fund;
    }

    public void setFund(double fund) {
        this.fund = fund;
    }


    @Override
    public String toString(){
        return "Brokerul " + this.getUsername() + " si id-ul " + this.getIdBroker() + " mai are suma de " + this.getFund();
    }
}
