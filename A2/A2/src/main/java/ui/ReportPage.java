package ui;

import business.BrokerBLL;
import business.ReportBLL;
import business.TransactionBLL;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
import javafx.stage.Window;
import persistence.Broker;

public class ReportPage extends Application {


    //private TextField startDatetf = new TextField("yyyy-mm-dd");
    //private TextField endDatetf = new TextField("yyyy-mm-dd");
    private TextField startDatetf = new TextField("2018-04-15");
    private TextField endDatetf = new TextField("2018-04-18");
    private Broker broker = new BrokerBLL("J").findBrokerById(2);
    public String typeOfConnection = "H";
    public String today = "2018-04-15";
    private Scene scene = new Scene(new Group());
    private Stage stage = new Stage();

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {


        primaryStage.setTitle("Logged as " + broker.getUsername());
        primaryStage.setWidth(300);
        primaryStage.setHeight(300);
        GridPane gridPane = new GridPane();
        this.addUIControls(gridPane);
        scene.setFill(Color.BEIGE);
        primaryStage.setScene(scene);
        stage = primaryStage;
        stage.show();


    }

    private void addUIControls(GridPane gridPane) {

        gridPane.setHgap(10);
        gridPane.setVgap(10);
        gridPane.relocate(25,-45);
        gridPane.setAlignment(Pos.CENTER);
        Label headerLabel = new Label("Details");
        headerLabel.setFont(Font.font("Arial", FontWeight.BOLD, 24));
        gridPane.add(headerLabel, 0, 0, 2, 1);
        GridPane.setHalignment(headerLabel, HPos.CENTER);
        GridPane.setMargin(headerLabel, new Insets(80, 0, 20, 0));


        Label dateLabel = new Label("Start date : ");
        gridPane.add(dateLabel, 0, 1);


        startDatetf.setPrefHeight(30);
        gridPane.add(startDatetf, 1, 1);

        Label quantityLabel = new Label("End date : ");
        gridPane.add(quantityLabel, 0, 2);


        endDatetf.setPrefHeight(30);
        gridPane.add(endDatetf, 1, 2);

        Button generateButton = new Button("Generate report from interval");
        gridPane.add(generateButton,1,4,2,1);


        Button generateeButton = new Button("Generate the daily report");
        gridPane.add(generateeButton,1,5,2,1);




        generateButton.setOnAction(event -> {
            ReportBLL reportBLL = new ReportBLL(typeOfConnection,"interval");
            String startDate = startDatetf.getText();
            String endDate = endDatetf.getText();
            int idBroker = this.broker.getIdBroker();
            reportBLL.getTransactionsFromInterval(startDate,endDate, idBroker);
            showAlert(Alert.AlertType.INFORMATION, gridPane.getScene().getWindow(), "End of the day!", "Report generated");

        });

        generateeButton.setOnAction(event -> {
            ReportBLL reportBLL = new ReportBLL(typeOfConnection,"daily");
            reportBLL.getDailyTransactions(today);
            showAlert(Alert.AlertType.INFORMATION, gridPane.getScene().getWindow(), "End of the day!", "Daily report generated");

        });

        ((Group) scene.getRoot()).getChildren().add(gridPane);
    }

    public Broker getBroker() {
        return broker;
    }

    public void setBroker(Broker broker) {
        this.broker = broker;
    }


    private void showAlert(Alert.AlertType alertType, Window owner, String title, String message) {
        Alert alert = new Alert(alertType);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.initOwner(owner);
        alert.show();
    }

}
