package ui;

import business.BrokerBLL;
import business.EndOfDayBLL;
import business.InstrumentBLL;
import business.TransactionBLL;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.TableView;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.Window;
import persistence.Broker;
import persistence.DailyReport;
import persistence.Instrument;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

import persistence.Transaction;

import java.text.SimpleDateFormat;
import java.util.Date;

public class BrokerPage extends Application {

    private Scene scene = new Scene(new Group());
    private TextField  nametf = new TextField();
    private  TextField pricetf = new TextField();
    private BrokerBLL bl = new BrokerBLL("J");
    public String connectionType;
    private Broker broker;
    private TransactionBLL tb = new TransactionBLL("J");
    private int selectedInstrument,selectedInstrumentShares;
    private TableView<Instrument> table = new TableView<>();
    private boolean isEndOfDay;


    public static void main(String[] args) {
        launch(args);
    }





    @Override
    public void start(Stage primaryStage) {
        Stage stage = new Stage();
        stage.setTitle("Logged as " + broker.getUsername());
        primaryStage.setWidth(600);
        primaryStage.setHeight(500);
        scene.setFill(Color.BEIGE);
        this.viewInstruments(table);
        primaryStage.setScene(scene);
        primaryStage.show();


    }


    private void viewInstruments(TableView<Instrument> table){

        GridPane gridPane = new GridPane();
        this.addUIControls2(gridPane);
        table.setEditable(true);
        TableColumn name = new TableColumn("Name");
        name.setCellValueFactory(new PropertyValueFactory<Instrument, String>("name"));

        TableColumn price = new TableColumn("Price");
        price.setCellValueFactory(new PropertyValueFactory<Instrument, Integer>("price"));

        TableColumn p = new TableColumn("Shares owned");
        p.setMinWidth(100);
        p.setCellValueFactory(new PropertyValueFactory<Instrument, Integer>("totalShares"));

        // loading data in Table
        InstrumentBLL insBll = new InstrumentBLL(this.connectionType);
        final ObservableList<Instrument> data = FXCollections.observableArrayList(insBll.findAll(this.broker.getIdBroker()));

        table.setItems(data);
        table.getColumns().addAll(name,price,p);
        table.relocate(300,50);
        table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

        //Selection Model
        table.getSelectionModel().selectedItemProperty().addListener((obs, ov, nv) -> {
            if (nv != null) {
                 Instrument instrument = table.getSelectionModel().getSelectedItem();
                 nametf.setText(instrument.getName());
                 pricetf.setText(String.valueOf(instrument.getPrice()));
                 this.selectedInstrument = instrument.getIdInstrument();
                 this.selectedInstrumentShares = instrument.getTotalShares();
                 //System.out.println(this.selectedInstrumentShares);
            }
        });
        ((Group) scene.getRoot()).getChildren().addAll(table,gridPane);
        table.setVisible(true);
    }

    private void addUIControls2(GridPane gridPane) {

        gridPane.relocate(10,-30);
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        Label headerLabel = new Label("Infos");
        headerLabel.setFont(Font.font("Arial", FontWeight.BOLD, 24));
        gridPane.add(headerLabel, 0, 0, 2, 1);
        GridPane.setHalignment(headerLabel, HPos.CENTER);
        GridPane.setMargin(headerLabel, new Insets(80, 0, 20, 0));


        Label nameLabel = new Label("Name : ");
        gridPane.add(nameLabel, 0, 1);


        nametf.setPrefHeight(30);
        nametf.setEditable(false);
        gridPane.add(nametf, 1, 1);

        Label quantityLabel = new Label("Quantity : ");
        gridPane.add(quantityLabel, 0, 2);

        TextField quantitytf = new TextField("0");
        quantitytf.setPrefHeight(30);

        gridPane.add(quantitytf, 1, 2);

        Label priceLabel = new Label("Price per share : ");
        gridPane.add(priceLabel, 0, 3);


        pricetf.setEditable(false);
        pricetf.setPrefHeight(30);
        gridPane.add(pricetf, 1, 3);

        Label priceTotalLabel = new Label("Total price : ");
        gridPane.add(priceTotalLabel, 0, 4);

        TextField priceTotaltf = new TextField("0");
        priceTotaltf.setEditable(false);
        priceTotaltf.setPrefHeight(30);
        gridPane.add(priceTotaltf, 1, 4);

        Label fundLabel = new Label(" My fund : ");
        gridPane.add(fundLabel, 0, 5);

        TextField fundtf = new TextField(String.valueOf(broker.getFund()) + "$");
        fundtf.setEditable(false);
        gridPane.add(fundtf,1,5);

        Button buyButton = new Button("Buy");
        buyButton.setPrefWidth(150);
        gridPane.add(buyButton,1,6,2,1);

        Button sellButton = new Button("Sell");
        sellButton.setPrefWidth(150);
        gridPane.add(sellButton,1,7,2,1);

        Button endButton = new Button("End of day");
        endButton.setPrefWidth(150);
        gridPane.add(endButton,1,9,2,1);

        Button tradesButton = new Button("My trades");
        tradesButton.setPrefWidth(150);
        gridPane.add(tradesButton,1,8,2,1);


        tradesButton.setOnAction(event ->{

            Stage secondStage = new Stage();
            BrokerTrades brokerTrades = new BrokerTrades();
            brokerTrades.typeConnection = this.connectionType;
            brokerTrades.setLoggedBroker(this.broker);
            brokerTrades.start(secondStage);
            secondStage.show();

        });

        quantitytf.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if(!quantitytf.getText().equals("") && !pricetf.getText().equals("")) {
                    int price = Integer.valueOf(pricetf.getText());
                    int quantity = Integer.valueOf(quantitytf.getText());
                    priceTotaltf.setText(String.valueOf(quantity * price));
                }
                else{
                    priceTotaltf.setText(String.valueOf(0));
                }
            }
        });

        TransactionBLL transactionBLL = new TransactionBLL(this.connectionType);
        buyButton.setOnAction(event ->{
            this.isEndOfDay = this.isTradingSessionOver();
            if(this.isEndOfDay == false) {
                System.out.println(this.getTimeofTheSystem());
                double currentFund = this.broker.getFund();
                int quantity = Integer.valueOf(quantitytf.getText());
                int price = Integer.valueOf(priceTotaltf.getText());
                double newFund = currentFund - price;
                if (newFund > 0) {
                    String todayDate = this.getTimeofTheSystem();
                    Transaction transaction = new Transaction(todayDate, this.broker.getIdBroker(), this.selectedInstrument, quantity, price, "buy",(int) newFund);
                    transactionBLL.insertTransaction(transaction);
                    this.broker.setFund(newFund);
                    fundtf.setText(String.valueOf(newFund));
                    this.bl.updateBroker(broker);
                    ((Group) scene.getRoot()).getChildren().removeAll(table,gridPane);
                    table = new TableView<>();
                    this.viewInstruments(this.table);

                } else {
                    showAlert(Alert.AlertType.ERROR, gridPane.getScene().getWindow(), "Error!", "Unavailable sum");
                }
            }
            else{
                showAlert(Alert.AlertType.ERROR, gridPane.getScene().getWindow(), "End of the day!", "The trading session is closed for today");
            }
        });

        sellButton.setOnAction(event ->{
            this.isEndOfDay = this.isTradingSessionOver();
            if(this.isEndOfDay == false) {
                double currentFund = this.broker.getFund();
                int quantity = Integer.valueOf(quantitytf.getText());
                int price = Integer.valueOf(priceTotaltf.getText());
                double newFund = currentFund + price;
                if(quantity < this.selectedInstrumentShares) {
                    String todayDate = this.getTimeofTheSystem();
                    this.selectedInstrumentShares -= quantity;
                    Transaction transaction = new Transaction(todayDate, this.broker.getIdBroker(), this.selectedInstrument, quantity, price, "sell",(int) newFund);
                    transactionBLL.insertTransaction(transaction);
                    this.broker.setFund(newFund);
                    fundtf.setText(String.valueOf(newFund));
                    this.bl.updateBroker(broker);
                    ((Group) scene.getRoot()).getChildren().removeAll(table,gridPane);
                    table = new TableView<>();
                    this.viewInstruments(this.table);
                }
                else{
                    showAlert(Alert.AlertType.ERROR, gridPane.getScene().getWindow(), "Error!", "Unavailable shares");
                }
            }
            else{
                showAlert(Alert.AlertType.ERROR, gridPane.getScene().getWindow(), "End of the day!", "The trading session is closed for today");
            }
        });

        endButton.setOnAction(event -> {
                EndOfDayBLL drb = new EndOfDayBLL(connectionType);
                String todayDate = this.getTimeofTheSystem();
                drb.insertDailyReport(new DailyReport(todayDate));
                Stage secondStage = new Stage();
                ReportPage reportPage = new ReportPage();
                reportPage.setBroker(this.broker);
                reportPage.typeOfConnection = connectionType;
                reportPage.today = todayDate;
                reportPage.start(secondStage);
                secondStage.show();
                showAlert(Alert.AlertType.INFORMATION, gridPane.getScene().getWindow(), "End of the day!", "The trading session is closed for today");

        });

    }

    private void showAlert(Alert.AlertType alertType, Window owner, String title, String message) {
        Alert alert = new Alert(alertType);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.initOwner(owner);
        alert.show();
    }


    public String getTimeofTheSystem(){
        String str ="";
        Date date = new Date(System.currentTimeMillis());
        try{
            String todayDate = new SimpleDateFormat("yyyy-MM-dd").format(date);
            str = todayDate;
            return todayDate;

        }catch(Exception e){
            e.printStackTrace();
        }
        return str;
    }

    public boolean isTradingSessionOver(){
        String todayDate = this.getTimeofTheSystem();
        EndOfDayBLL drb = new EndOfDayBLL(connectionType);
        return drb.endOfDay(todayDate);
    }

    public Broker getBroker() {
        return broker;
    }

    public void setBroker(Broker broker) {
        this.broker = broker;
    }
}
