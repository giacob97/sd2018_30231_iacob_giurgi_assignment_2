package ui;

import business.BrokerBLL;
import business.InstrumentBLL;
import business.TransactionBLL;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

import javafx.stage.Stage;
import persistence.Broker;
import persistence.Transaction;

import java.util.Date;
@SuppressWarnings("all")
public class BrokerTrades extends Application {

    private Scene scene = new Scene(new Group());
    private TextField datetf = new TextField();
    private TextField instrumenttf = new TextField();
    private TextField typetf = new TextField();
    private TextField quantitytf = new TextField();
    private TextField priceTotaltf = new TextField();
    public String typeConnection;
    private int idTransaction,idInstrument,idBroker,newFund;
    private TableView<Transaction> table = new TableView<>();
    private BrokerBLL brokerBLL;
    private Broker loggedBroker;

    public static void main(String[] args) {
        launch(args);
    }


    @Override
    public void start(Stage primaryStage) {
        Stage stage = new Stage();
        stage.setTitle("Logged");
        primaryStage.setWidth(900);
        primaryStage.setHeight(500);
        scene.setFill(Color.BEIGE);

        this.viewTrades(table);
        primaryStage.setScene(scene);
        primaryStage.show();
    }




    private void viewTrades(TableView<Transaction> table){
        table.setEditable(true);
        GridPane gridPane = new GridPane();
        this.addUIControls(gridPane);
        gridPane.relocate(10,-30);

        TableColumn dateColumn = new TableColumn("Date");
        dateColumn.setMinWidth(100);
        dateColumn.setCellValueFactory(new PropertyValueFactory<Transaction, Date>("date"));

        TableColumn instrumentColumn = new TableColumn("Instrument");
        instrumentColumn.setMinWidth(100);
        instrumentColumn.setCellValueFactory(new PropertyValueFactory<Transaction, Integer>("idInstrument"));


        TableColumn quantityColumn = new TableColumn("Quantity");
        quantityColumn.setMinWidth(100);
        quantityColumn.setCellValueFactory(new PropertyValueFactory<Transaction , Integer>("quantity"));

        TableColumn priceColumn = new TableColumn("Price");
        priceColumn.setMinWidth(100);
        priceColumn.setCellValueFactory(new PropertyValueFactory<Transaction, Integer>("price"));


        TableColumn type = new TableColumn("Type");
        type.setMinWidth(175);
        type.setCellValueFactory(new PropertyValueFactory<Transaction, String>("type"));
        TransactionBLL tb = new TransactionBLL(typeConnection);
        final ObservableList<Transaction> data = FXCollections.observableArrayList(tb.findAllByBroker(this.loggedBroker.getIdBroker()));
        table.setItems(data);
        table.getColumns().addAll(dateColumn,instrumentColumn,quantityColumn, priceColumn,type);
        table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        table.relocate(300,50);

        table.getSelectionModel().selectedItemProperty().addListener((obs, ov, nv) -> {
            if (nv != null) {
                Transaction transaction = table.getSelectionModel().getSelectedItem();
                this.idInstrument = transaction.getIdInstrument();
                InstrumentBLL instrumentBLL = new InstrumentBLL(typeConnection);
                String instrumentName = instrumentBLL.findInstrumentById(this.idInstrument).getName();
                datetf.setText(transaction.getDate().toString());
                quantitytf.setText(String.valueOf(transaction.getQuantity()));
                priceTotaltf.setText(String.valueOf(transaction.getPrice()));
                typetf.setText(transaction.getType());
                instrumenttf.setText(instrumentName);
                this.idTransaction = transaction.getIdTransaction();
                this.idBroker = transaction.getIdbroker();
            }
        });

        ((Group) scene.getRoot()).getChildren().addAll(table,gridPane);
        table.setVisible(true);
    }

    private void addUIControls(GridPane gridPane) {

        gridPane.relocate(5,0);
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        Label headerLabel = new Label("Infos");
        headerLabel.setFont(Font.font("Arial", FontWeight.BOLD, 24));
        gridPane.add(headerLabel, 0, 0, 2, 1);
        GridPane.setHalignment(headerLabel, HPos.CENTER);
        GridPane.setMargin(headerLabel, new Insets(80, 0, 20, 0));


        Label dateLabel = new Label("Date : ");
        gridPane.add(dateLabel, 0, 1);


        datetf.setPrefHeight(30);
        datetf.setEditable(false);
        gridPane.add(datetf, 1, 1);

        Label quantityLabel = new Label("Quantity : ");
        gridPane.add(quantityLabel, 0, 2);


        quantitytf.setPrefHeight(30);
        gridPane.add(quantitytf, 1, 2);

        Label insLabel = new Label("Instrument : ");
        gridPane.add(insLabel, 0, 3);


        instrumenttf.setEditable(false);
        instrumenttf.setPrefHeight(30);
        gridPane.add(instrumenttf, 1, 3);


        Label priceTotalLabel = new Label("Total price : ");
        gridPane.add(priceTotalLabel, 0, 4);

        priceTotaltf.setPrefHeight(30);
        gridPane.add(priceTotaltf, 1, 4);


        Label typeLabel = new Label("Type");
        gridPane.add(typeLabel,0,5);


        typetf.setEditable(false);
        typetf.setPrefHeight(30);
        gridPane.add(typetf, 1, 5);

        Button update = new Button("Update");
        update.setPrefWidth(150);
        gridPane.add(update,1,6,2,1);

        Button dellButton = new Button("Delete");
        dellButton.setPrefWidth(150);
        gridPane.add(dellButton,1,7,2,1);



        TransactionBLL tb = new TransactionBLL(this.typeConnection);
        update.setOnAction(event -> {
            String date = this.datetf.getText().toString();
            int quantity = Integer.valueOf(quantitytf.getText());
            int price = Integer.valueOf(priceTotaltf.getText());
            String type = this.typetf.getText();
            this.brokerBLL = new BrokerBLL(this.typeConnection);
            Broker broker = this.brokerBLL.findBrokerById(idBroker);
            Transaction transaction = new Transaction(date,idBroker,idInstrument,quantity,price,type,(int) broker.getFund());
            transaction.setIdTransaction(this.idTransaction);
            tb.updateTransaction(transaction);
            ((Group) scene.getRoot()).getChildren().removeAll(table,gridPane);
            table = new TableView<>();
            this.viewTrades(this.table);
        });

        dellButton.setOnAction(event ->{
            tb.deleteTransaction(this.idTransaction);
        });



    }


    public Broker getLoggedBroker() {
        return loggedBroker;
    }

    public void setLoggedBroker(Broker loggedBroker) {
        this.loggedBroker = loggedBroker;
    }

    public Scene getScene() {
        return scene;
    }

    public void setScene(Scene scene) {
        this.scene = scene;
    }
}
