package ui;

import business.BrokerBLL;
import javafx.application.Application;
import javafx.scene.layout.GridPane;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import persistence.Broker;

public class Login extends Application {

    public static void main(String[] args) {
        launch(args);
    }


    private String connectionType;

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Trading Application");
        primaryStage.setHeight(250);
        primaryStage.setWidth(450);
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));

        Text scenetitle = new Text("Log in");
        scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
        grid.add(scenetitle, 0, 0, 2, 1);

        Label userName = new Label("Username:");
        grid.add(userName, 0, 1);

        final TextField userTextField = new TextField();
        grid.add(userTextField, 1, 1);

        Label pw = new Label("Password:");
        grid.add(pw, 0, 2);

        final PasswordField pwBox = new PasswordField();
        grid.add(pwBox, 1, 2);

        Button btn = new Button("Sign in using Hibernate");
        Button btn1 = new Button("Sign in using JDBC");
        Button createButton = new Button("Create account");
        createButton.setPrefWidth(265);
        HBox hbBtn = new HBox(10);
        hbBtn.setAlignment(Pos.BOTTOM_RIGHT);
        hbBtn.getChildren().addAll(btn,btn1);
        grid.add(hbBtn, 1, 4);
        grid.add(createButton,1,5);
        final Text actiontarget = new Text();
        grid.add(actiontarget, 1, 6);
        btn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                BrokerBLL bl= new BrokerBLL("H");
                Broker b = bl.findBrokerByUsername(userTextField.getText());
                if( b.getPassword().equals(pwBox.getText())){
                    System.out.println("Corect");
                    BrokerPage brokerPage = new BrokerPage();
                    brokerPage.setBroker(b);
                    brokerPage.connectionType = "H";
                    primaryStage.setTitle("Logged as " + b.getUsername());
                    brokerPage.start(primaryStage);
                }
                else{
                    actiontarget.setFill(Color.FIREBRICK);
                    actiontarget.setText("Wrong Password");
                    actiontarget.setTextAlignment(TextAlignment.CENTER);
                }
            }
        });

        btn1.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                BrokerBLL bl= new BrokerBLL("J");
                Broker b = bl.findBrokerByUsername(userTextField.getText());
                if( b.getPassword().equals(pwBox.getText())){
                    System.out.println("Corect");
                    BrokerPage brokerPage = new BrokerPage();
                    brokerPage.setBroker(b);
                    brokerPage.connectionType = "J";
                    primaryStage.setTitle("Logged as " + b.getUsername());
                    brokerPage.start(primaryStage);
                }
                else{
                    actiontarget.setFill(Color.FIREBRICK);
                    actiontarget.setText("Wrong Password");
                    actiontarget.setTextAlignment(TextAlignment.CENTER);
                }
            }
        });

        createButton.setOnAction(event ->{
            Stage secondStage = new Stage();
            NewBrokerAccount newBrokerAccount = new NewBrokerAccount();
            newBrokerAccount.start(secondStage);
        });

        Scene scene = new Scene(grid, 300, 275);
        scene.setFill(Color.BEIGE);
        primaryStage.setScene(scene);
        primaryStage.show();

    }
}
