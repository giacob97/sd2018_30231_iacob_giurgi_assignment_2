package business;

import factory.*;
import persistence.Transaction;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

public class ReportBLL {

    private ReportInterface reportInterface;
    private String typeOfConnection;
    private  List<Transaction> transactions;


    public ReportBLL(String typeOfConnection ,String typeOfReport){
        this.typeOfConnection = typeOfConnection;
        if (typeOfReport.equals("daily")){
            reportInterface = new DailyReportDao();
        }
        else{
            if(typeOfReport.equals("interval")){
                reportInterface = new IntervalReportsDao();
            }
            else {
                reportInterface = null;
            }
        }
    }

    public void getDailyTransactions(String todayDate){

        try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("Today trades.txt"), "utf-8"))) {
            transactions = new ArrayList<>();
            transactions = this.reportInterface.getTransactions(typeOfConnection,todayDate,"",0);
            for(Transaction t : transactions){
                writer.write(t.toString());
                writer.newLine();
            }
        }catch (IOException e){
            e.printStackTrace();
        }

    }


    public void getTransactionsFromInterval(String startDate,String endDate,int idBroker){

        try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(typeOfConnection + " Transactions from " + startDate + " to " + endDate + ".txt"), "utf-8"))) {
            transactions = new ArrayList<>();
            transactions = this.reportInterface.getTransactions(typeOfConnection,startDate,endDate,idBroker);
            for(Transaction t : transactions){
                writer.write(t.toString());
                writer.newLine();
            }
        }catch (IOException e){
            e.printStackTrace();
        }
    }


    public String getTypeOfConnection() {
        return typeOfConnection;
    }

    public void setTypeOfConnection(String typeOfConnection) {
        this.typeOfConnection = typeOfConnection;
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<Transaction> transactions) {
        this.transactions = transactions;
    }
}
