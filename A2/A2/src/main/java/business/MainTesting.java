package business;


import org.hibernate.Hibernate;
import persistence.Broker;
import persistence.Instrument;
import persistence.Transaction;
import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

///https://gist.github.com/prakashdayal/4699481
public class MainTesting {

    @Test
    public void insertIntrument(){
        Instrument instrument = new Instrument();
        instrument.setIdInstrument(44);
        instrument.setName("Bitcoin");
        instrument.setPrice(6900);
        InstrumentBLL instrumentBLL = new InstrumentBLL("H");
        int size = instrumentBLL.findAllFromDB().size();
        instrumentBLL.insertInstrument(instrument);
        assertEquals(size+1,instrumentBLL.findAllFromDB().size());
    }

    @Test
    public void deleteBroker(){
        Broker broker = new Broker();
        broker.setIdBroker(7);
        BrokerBLL brokerBLL = new BrokerBLL("H");
        int size = brokerBLL.findAll().size() - 1;
        brokerBLL.deleteBroker(broker.getIdBroker());
        assertEquals(size , brokerBLL.findAll().size());
    }


    @Test
    public void findTransactionById(){
        TransactionBLL transaction = new TransactionBLL("H");
        int id = 2;
        Transaction tr = transaction.findTransactionById(1);
        boolean hasBeenFound = Hibernate.isInitialized(tr.getDate());
        assertTrue(hasBeenFound);
    }

    @Test
    public void checkTransactionForAInstrumentOnADailyReport(){
        int idInstrument = 2;
        ReportBLL reportBLL = new ReportBLL("H","daily");
        reportBLL.getDailyTransactions("2018-04-16");
        List<Transaction> transactionList = reportBLL.getTransactions();
        List<Integer> idInstruments = new ArrayList<>();
        for(Transaction transaction : transactionList){
            idInstruments.add(transaction.getIdInstrument());
        }
        assertNotNull(idInstruments.contains(idInstrument));
    }


    @Test
    public void findByUsername(){
        BrokerBLL brokerBLL = new BrokerBLL("J");
        Broker broker = brokerBLL.findBrokerByUsername("giacob");
        System.out.println(broker.getUsername());
        assertNotNull(broker.getUsername());
    }




    @Test
    public void deleteInstrument(){
        InstrumentBLL instrumentBLL = new InstrumentBLL("J");
        int size = instrumentBLL.findAllFromDB().size() - 1;
        instrumentBLL.deleteInstrument(6);
        assertEquals(size , instrumentBLL.findAllFromDB().size());
    }

    @Test
    public void updateTransaction(){
        TransactionBLL transactionBLL = new TransactionBLL("J");
        Transaction transaction = transactionBLL.findTransactionById(1);
        int beforeUpdate = transaction.getPrice();
        transaction.setPrice(6850);
        transactionBLL.updateTransaction(transaction);
        transaction = transactionBLL.findTransactionById(1);
        int afterUpdate = transaction.getPrice();
        assertNotSame(beforeUpdate,afterUpdate);
        }

    @Test
    public void isEndOfDay(){
        EndOfDayBLL endOfDayBLL = new EndOfDayBLL("J");
        boolean expected = true;
        assertSame(expected , endOfDayBLL.endOfDay("2018-04-15"));
    }
}
