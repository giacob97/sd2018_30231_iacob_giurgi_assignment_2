package business;

import factory.DailyReportDaoFactory;
import factory.EndOfDayInterface;
import persistence.DailyReport;

import java.util.List;

public class EndOfDayBLL {

    private EndOfDayInterface dri;
    private String typeOfConnection;


    public EndOfDayBLL(String type){
        DailyReportDaoFactory dailyReportDaoFactory = new DailyReportDaoFactory();
        this.dri = dailyReportDaoFactory.getDailyReport(type);
    }

    public DailyReport findDailyReportById(int id) {
        return dri.findById(id);
    }


    public boolean endOfDay(String today){
        return dri.endOfDay(today);
    }


    public List<DailyReport> findAll() {
        return dri.findAll();
    }


    public DailyReport insertDailyReport(DailyReport dailyReport) {
        return dri.insert(dailyReport);
    }


    public DailyReport updateDailyReport(DailyReport dailyReport) {

        return dri.update(dailyReport);
    }


    public DailyReport deleteDailyReport(int id) {

        return dri.delete(id);
    }



}
