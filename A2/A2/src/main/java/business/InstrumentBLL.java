package business;

import factory.FactoryInterface;
import factory.InstrumentDaoFactory;
import persistence.Broker;
import persistence.Instrument;

import java.util.List;

public class InstrumentBLL {

    FactoryInterface<Instrument> instrumentDAO;
    private String typeOfConnection;


    public InstrumentBLL(){

    }

    public InstrumentBLL(String type){
        InstrumentDaoFactory idf = new InstrumentDaoFactory();
        this.instrumentDAO = idf.getInstrument(type);
        this.typeOfConnection = type;
    }

    public Instrument findInstrumentById(int id) {
        return this.instrumentDAO.findById(id);
    }

    public List<Instrument> findAllFromDB(){
         return this.instrumentDAO.findAll();
    }



    public List<Instrument> findAll(int id) {
        List<Instrument> finals = this.instrumentDAO.findAll();
        TransactionBLL tb = new TransactionBLL(typeOfConnection);
        for(Instrument instrument : finals){
            instrument.setTotalShares(tb.findAllTransactionsByInstrument(instrument.getIdInstrument(),id));
        }
        return finals;
    }


    public Instrument insertInstrument(Instrument instrument) {
        return this.instrumentDAO.insert(instrument);
    }


    public Instrument updateInstrument(Instrument instrument) {

        return this.instrumentDAO.update(instrument);
    }


    public Instrument deleteInstrument(int id) {

        return this.instrumentDAO.delete(id);
    }

}
