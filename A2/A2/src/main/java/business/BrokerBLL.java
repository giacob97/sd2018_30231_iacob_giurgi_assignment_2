package business;

import factory.BrokerDaoFactory;
import factory.FactoryInterface;
import persistence.Broker;

import java.util.List;

public class BrokerBLL {

    FactoryInterface<Broker> brokerDAO;

    public BrokerBLL(){

    }

    public BrokerBLL(String type){
          BrokerDaoFactory bf = new BrokerDaoFactory();
          this.brokerDAO = bf.getBroker(type);
    }

    public Broker findBrokerById(int id) {
        Broker brokerById = brokerDAO.findById(id);
        return brokerById;
    }

    public Broker findBrokerByUsername(String username) {
        return this.brokerDAO.findByUsername(username);
    }


    public List<Broker> findAll() {
        List<Broker> brokers = this.brokerDAO.findAll();
        return brokers;
    }


    public Broker insertBroker(Broker broker) {
        return brokerDAO.insert(broker);
    }


    public Broker updateBroker(Broker broker) {

        return brokerDAO.update(broker);
    }


    public Broker deleteBroker(int id) {

        return brokerDAO.delete(id);
    }


}
