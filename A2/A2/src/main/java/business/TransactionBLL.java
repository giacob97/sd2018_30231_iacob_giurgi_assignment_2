package business;

import factory.*;
import persistence.Transaction;

import java.io.*;
import java.util.List;
import java.util.stream.Collectors;

public class TransactionBLL {

    FactoryInterface<Transaction> transactionDao;
    private String typeOfConnection;
    private TransactionFactoryInterface transactionFactoryInterface;

    public TransactionBLL() {
    }

    public TransactionBLL(String type){
        TransactionDaoFactory tdf = new TransactionDaoFactory();
        this.transactionDao = tdf.getTransaction(type);
        this.typeOfConnection = type;

    }

    public Transaction findTransactionById(int id) {
        return this.transactionDao.findById(id);
    }


    public List<Transaction> findAll() {
        return this.transactionDao.findAll();
    }

    public List<Transaction> findAllByBroker(int id){
        List<Transaction> initialList = this.findAll();
        List<Transaction> myTrades = initialList.stream().filter(x -> x.getIdbroker() == id).collect(Collectors.toList());
        return myTrades;
    }


    public Transaction insertTransaction(Transaction transaction) {
        return this.transactionDao.insert(transaction);
    }


    public Transaction updateTransaction(Transaction transaction) {

        return this.transactionDao.update(transaction);
    }


    public Transaction deleteTransaction(int id) {

        return this.transactionDao.delete(id);
    }

     public int findAllTransactionsByInstrument(int id,int idBroker){

        if(typeOfConnection.equals("J")){
            transactionFactoryInterface = new TransactionDaoJDBC();
            return transactionFactoryInterface.getNumberOfSharesPerInstrument(id,idBroker);
        }
        else{
            transactionFactoryInterface = new TransactionDaoHibernate();
            return transactionFactoryInterface.getNumberOfSharesPerInstrument(id,idBroker);
        }
     }




}
