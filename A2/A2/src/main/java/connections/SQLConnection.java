package connections;

import factory.TransactionDaoJDBC;

import java.sql.*;
import java.util.logging.Logger;

public class SQLConnection implements  ConnectionFactory{


    @SuppressWarnings("unused")
    private static final Logger LOGGER = Logger.getLogger(SQLConnection.class.getName());
    private static final String DRIVER = "com.mysql.jdbc.Driver";
    private static final String DBURL = "jdbc:mysql://localhost:3306/tradingbd?autoReconnect=true&useSSL=false";
    private static final String USER = "root";
    private static final String PASS = "";

    public static SQLConnection singleInstance = new SQLConnection();
    Connection connection ;

    private SQLConnection(){
        try{
            Class.forName(DRIVER);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private Connection createConnection(){
        connection = null;
        try {
            connection = (Connection) DriverManager.getConnection(DBURL,USER,PASS);
            //System.out.println("Ne-am conectat cu succes la baza de date pentru trading");
            return connection ;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Connection getConnection(){
        return singleInstance.createConnection();
    }

    public static void close(Connection connection){
        try {
            connection.close();
        } catch (SQLException e) {
            System.out.println("Nu s-a inchis corect Conexiunea!");
        }
    }

    public static void close(PreparedStatement s){
        try {
            s.close();
        } catch (SQLException e) {
            System.out.println("Nu s-a inchis corect Statement-ul!");
        }
    }


    public static void close(ResultSet rs){
        try {
            rs.close();
        } catch (SQLException e) {
            System.out.println("Nu s-a inchis corect ResultSet-ul!");
        }
    }

    public static void main(String[] args){

        //DailyReport dailyReport = new DailyReport("2014-06-05");
        //EndOfDayDaoJDBC drb = new EndOfDayDaoJDBC();
        //System.out.println(drb.endOfDay("2014-05-05"));

        /*
        BrokerDaoJDBC bd = new BrokerDaoJDBC();
        Broker b = new Broker();
        //Broker b = new Broker("giacob","pass");
        //System.out.println(bd.delete(2).toString());
        //System.out.println(bd.findAll().size());
        b.setIdBroker(2);
        b.setFund(555000);
        bd.update(b);
        */
        /*
        InstrumentDaoJDBC id = new InstrumentDaoJDBC();
        Instrument instrument = new Instrument("Micro",2500);
        System.out.println(id.findAll().size());
        id.delete(3);
        System.out.println(id.findAll().size());
        id.update(instrument);
        id.insert(instrument);
        System.out.println(id.findById(3).toString());
        */

        TransactionDaoJDBC td = new TransactionDaoJDBC();
        System.out.println(td.getTransactionsFromInterval("2018-04-15","2018-04-16",2).size());
        /*
        Transaction transaction = new Transaction("2012-08-15",2,3,10,4440,"sell");
        //td.insert(transaction);
        //System.out.println(td.findById(5).toString());
        //transaction.setIdTransaction(3);
        //transaction.setQuantity(55);
        //transaction.setPrice(74589);
        //td.update(transaction);
        System.out.println(td.findAll().size());
        td.delete(4);
        */


    }

}
