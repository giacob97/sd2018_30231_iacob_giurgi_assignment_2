package connections;

import factory.TransactionDaoHibernate;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateConnection implements ConnectionFactory{

    public HibernateConnection() {

    }

    public SessionFactory initialize() {
        SessionFactory sessionFactory=new Configuration()
                .configure("hibernate.cfg.xml")
                .buildSessionFactory();
        return sessionFactory;
    }

    public static void main(String [] args){

        //EndOfDayDaoHibernate drb = new EndOfDayDaoHibernate();
        //System.out.println(drb.endOfDay("2014-05-05"));
        //drb.insert(new DailyReport("2018-02-09"));

        /*b.setIdBroker(3);
        b.setFund(440055);
        //bd.insert(b);
        //System.out.println(bd.findAll().size());
        //System.out.println(bd.findById(2).toString());
        //bd.update(b);
        bd.delete(4);
        */
        /*
        InstrumentDaoHibernate id = new InstrumentDaoHibernate();
        Instrument instrument = new Instrument("Micro",2500);
        instrument.setIdInstrument(1);
        instrument.setPrice(58000);
        //System.out.println(id.findAll().size());
        id.delete(4);
        //System.out.println(id.findAll().size());
        //id.update(instrument);
        //id.insert(instrument);
        //System.out.println(id.findById(4).toString());
        */

        TransactionDaoHibernate td = new TransactionDaoHibernate();
        System.out.println(td.getTransactionsFromInterval("2018-04-15","2018-04-16",2).size());
        //Transaction transaction = new Transaction("2017-08-15",2,1,44,3000,"sell");
        //td.insert(transaction);
        //System.out.println(td.findById(2).getType());
        //transaction.setIdTransaction(1);
        //transaction.setQuantity(15);
        //transaction.setPrice(6850);
        //td.update(transaction);
        //System.out.println(td.findAll().size());
        //td.delete(7);

    }

}
